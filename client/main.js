import { Template } from 'meteor/templating';
import { Applications } from '../collections/applications';

import './main.html';

Template.applications.onCreated(function applicationsOnCreated() {
  Meteor.subscribe('applications');
});

Template.applications.helpers({
  applications() {
    return Applications.find()
  },
  count(regex) {
    return Applications.find({text: {$regex: regex, $options: 'ig'}}).count()
  }
});

Template.applications.events({
  'submit form#add_form'(event) {
    event.preventDefault();
    const target = event.target;
    const text = target.company.value;
    Meteor.call('application.add', {text, date: new Date()});
    target.company.value = '';
  }
});
Template.scripts.onRendered(function() {
    $(document).ready(function() {
        var script = document.createElement("script");
        script.type="text/javascript";
        script.src = "https://www.googletagmanager.com/gtag/js?id=UA-118289911-1";
        $("#script_div").append(script);

        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-118289911-1');
    });
    $(document).ready(function() {
        var script = document.createElement("script");
        script.type="text/javascript";
        script.src = "https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js";
        $("#script_div").append(script);
    });
    $(document).ready(function() {
        var script = document.createElement("script");
        script.type="text/javascript";
        script.async = "true";
        script.src = "https://yastatic.net/share2/share.js";
        $("#script_div").append(script);
    });

});
