import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

export const Applications = new Mongo.Collection('applications');

Applications.schema = new SimpleSchema({
    'text': { type: String, min: 3, max: 50},
    'date': { type: Date }
});

Meteor.methods({
    'application.add'({text, date}) {
        Applications.schema.validate({ text, date })
        Applications.insert({ text, date })
    }
})

