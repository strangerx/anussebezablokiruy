import { Meteor } from 'meteor/meteor';
import '../collections/applications';
import {Applications} from "../collections/applications";

Meteor.startup(() => {
    Meteor.publish('applications', ()=>Applications.find())
});
